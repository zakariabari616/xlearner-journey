
# GIT Basics

## About

* Version Control System (VCS)
* Coordination between developers
* Revert back at any time

## Concepts

* Track of code history
* Visit any snapshot at anytime
* Stage files before committing
----

## Basic commands

### Configure

```git
$ git config --global user.name 'Zakarya Bari'
```
Add user name to ***staging area*** 
```git
$ git config --global user.email 'zakariabari616@gmail.com'
```
Add user email to ***staging area***

### Working with Repositories

```git
$ git init
```
Initialize local ***git*** Repository

```git
$ git clone https://gitlab.com/zakariabari616/xlearner-journey.git
```
Download from ***Remote Repository*** (ex: ***GitLab***)

```git
$ git remote add origine https://gitlab.com/zakariabari616/xlearner-journey.git
```
Initialize the ***Remote Repository*** and connect it to the local one

```git
$ git push -u origine master
```
Push the changes to the repository's main ***Branch***

### Working with Branches

```git
$ git branch branchName
```
Create new ***Branch***

```git
$ git checkout branchName
```
Switch to another ***Branch***

```git
$ git merge branchName
```
Merge new ***Branch*** changes to the ***master*** one

### Modifications

```git
$ git log
```
Shows history of current ***Branch***

```git
$ git status
```
Check status of ***Workiing Tree***

```git
$ git add <file>
```
Add file to the ***Index***

```git
$ git add *.extension
```
Add all files of giving extension to the ***staging area***

```git
$ git add .
```
Add all files to the ***staging area***

```git
$ git commit -m 'message'
```
Commit changes in ***Index***

### Synchronization

```git
$ git push
```
Upload all local changes to the ***Remote Repository***

```git
$ git pull
```
Pull latest changes from the ***Remote Repository***

```git
$ git merge
```
Integrate changes from another ***Branch***

### .gitingnore file

```git
$ touch .gitignore
```
Ban committing any file included by name or /directory inside the gitignore file.

### Other commands

```git
$ clear
```
Clear git command window

```git
$ touch
```
Create a file

----

## Key-Words

**Repository**
>The directory that stores all the files, folders, and content needed for your project.

**Staging area / Index**
>Files that have been changed, until you are ready to commit the files.

**HEAD**
> used to denote the most current commit of the repository in which you are working.

**Fork**
>Creates a copy of a repository.

**Fetch**
>Downloading and copying that branch’s files to your workstation.

